import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { PlanningComponent } from './planning/planning.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { InterventionComponent } from './intervention/intervention.component';
import { PlanningContainerComponent } from './planning-container/planning-container.component';

@NgModule({
    declarations: [
        AppComponent,
        PlanningComponent,
        InterventionComponent,
        PlanningContainerComponent,
    ],
    imports: [
        BrowserModule,
        CommonModule,
        DragDropModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
