export class Row {
    public id: number;
    public value: string;

    public constructor(id: number, value: string) {
        this.id = id;
        this.value = value;
    }
}

export class Column {
    public id: number;
    public value: string;

    public constructor(id: number, value: string) {
        this.id = id;
        this.value = value;
    }
}
