import { Component } from '@angular/core';

@Component({
  selector: 'tl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './calendar.component.scss']
})
export class AppComponent {
  title = 'TimeLine';
  hours = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
  jm = [1, 2, 3, 4, 5];

}
