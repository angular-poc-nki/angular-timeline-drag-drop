import { Injectable } from '@angular/core';
import { Column, Row } from '../shared/planning.model';

@Injectable({ providedIn: 'root' })
export class PlanningService {
    private static readonly TIMELINE_START = 6;
    private static readonly TIMELINE_END = 22;
    private static readonly COLUMNS_HEADERS = [
        'Toto',
        'Tata',
        'Tonton'
    ];

    public getRows(): Row[] {
        return Array(PlanningService.TIMELINE_END - PlanningService.TIMELINE_START + 1)
            .fill(undefined).map((_, idx) =>
                new Row(idx, `${PlanningService.TIMELINE_START + idx}`)
            );
    }

    public getColumns(): Column[] {
        return PlanningService.COLUMNS_HEADERS.map((value, idx) =>
            new Column(idx, value)
        );
    }
}
