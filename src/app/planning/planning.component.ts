import { Component, Input } from '@angular/core';
import { CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import { Column, Row } from '../shared/planning.model';

@Component({
  selector: 'tl-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent {
  @Input()
  public rows: Row[];

  @Input()
  public columns: Column[];

  hours = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];
  bac_a_sable = [];
  techniciens = [
    {
      id: 1,
      name: 'Toto',
      interventions: [
        {
          id: 1,
          timeStart: 10,
          timeEnd: 15
        },
        {
          id: 3,
          timeStart: 17,
          timeEnd: 20
        }
      ]
    },
    {
      id: 2,
      name: 'Tata',
      interventions: [
        {
          id: 2,
          timeStart: 8,
          timeEnd: 12
        }
      ]
    },
    {
      id: 3,
      name: 'Tonton',
      interventions: []
    },
  ];

  // Deuxième attribut sans doutes inutile
  onDrop(event: CdkDragDrop<string[]>, from) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
    }
  }

  public getRowStyle(i: number) {
    return {
      'grid-area': `${i}/1/${i}/18`,
    };
  }

  public getScheduleStyle() {
    /*
          grid-template-columns: repeat($nombre_horaires, minmax(0, 1fr));
      grid-template-rows: repeat($nombre_techos, minmax(0, 1fr));
     */
    return {
      'grid-template-columns': `repeat(${this.hours.length}, minmax(0, 1fr)`,
      'grid-template-rows': `repeat(${this.techniciens.length}, minmax(0, 1fr)`
    };
  }

  public getGridTechosStyle() {
    return {
      'grid-template-rows': `repeat(${this.techniciens.length}, minmax(0, 1fr)`
    };
  }

  /**
   * Méthode permettant de vérifier si deux interventions se croise
   * On place l'élement en fonction de son index
   * @param interCourante
   * @param listeInterventionDuTechos
   * @returns {string}
   */
  public getHoraire(interCourante, listeInterventionDuTechos): string {
    listeInterventionDuTechos.sort((a, b) => a.timeStart - b.timeStart); // On ordonne les interventions dans l'ordre croissant à partir de l'heure de début
    const index = listeInterventionDuTechos.indexOf(interCourante); // On récupère l'index de l'intervention courante pour les ordonner verticalement
    if (this.check(listeInterventionDuTechos[index], listeInterventionDuTechos[index + 1])
        || this.check(listeInterventionDuTechos[index], listeInterventionDuTechos[index - 1])) {
      return `${index + 1} / ${(interCourante.timeStart - 5)} / ${index + 1} / ${(interCourante.timeEnd - 5)}`;
    }
    return `1 / ${(interCourante.timeStart - 5)} / ${index + 1} / ${(interCourante.timeEnd - 5)}`;
  }

  private check(interA, interB) {
    if (interB === undefined || interB === null) {
      return false;
    } else {
      return interA.timeStart < interB.timeEnd && interA.timeEnd > interB.timeStart;
    }
  }
}
