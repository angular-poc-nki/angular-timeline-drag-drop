import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'tl-intervention',
  templateUrl: './intervention.component.html',
  styleUrls: ['./intervention.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InterventionComponent {
  @Input()
  public technicien;

  @Input()
  public inter;
}
